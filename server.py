import bottle

from guessTheSubnet import make_question, check_IP_validity

@bottle.route("/")
@bottle.route("/index")
def index():
    return bottle.static_file("index.html", root=".")

@bottle.route("/new_question")
def displayTable():
    hint, answer = make_question()
    return '{"hint": "%s", "answer": "%s"}' % (hint, answer)

@bottle.route("/check_validity")
def check_validity():
    rest_args = bottle.request.query
    sample_ip = rest_args.sample_ip
    is_valid = check_IP_validity(sample_ip)
    if is_valid:
        return '{"IP_is_valid": "yes"}'
    else:
        return '{"IP_is_valid": "no"}'

if __name__ == '__main__':
    bottle.run(debug=True, reloader=True)
