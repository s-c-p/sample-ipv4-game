#import modules
import ipaddress
import random

def check_IP_validity(samp_ip):
    try:
        parsed_ip = ipaddress.ip_network(samp_ip)
    except ValueError:
        return False
    return parsed_ip

def read_user_input():
    while True:
        userAnswer = input("This IP is in network: ")
        parsed_ip = check_IP_validity(userAnswer)
        if parsed_ip:
            break
        else:
            print("Not a valid IP network! Try again.\n")
    return parsed_ip

def make_question():
    #Generates a random 32 bit number, generates a randome CIDR 1-30, converts 32-bit number to IP, converts IP and CIDR to string, 
    #concatenates the two strings, and converts string to interface IP
    
    randoIP = random.getrandbits(32)
    randoSub = random.randrange(1,30,1)
    convertIP = ipaddress.IPv4Address(randoIP)
    netToString = str(randoSub)
    ipToString = str(convertIP)
    guessIPString = ipToString + '/' + netToString
    convertIP = ipaddress.ip_interface(guessIPString)

    #Sets guessIPNetwork to Correct Answer by changing convertIP into the network
    guessIPNetwork = convertIP.network
    return guessIPString, guessIPNetwork

def guessSubnet():
    hint, answer = make_question()
    print('What is the network of ', hint, '\n(Enter your answer in X.X.X.X/XX format)\n')

    #checks answer against network of random IP
    if read_user_input() == answer:
        print('\nYES! You got it, rock-star!')
    else:
        print()
        print("Mission failed. The hospital crashed, now you're fired.".center(79))
        print('(The correct answer is: ', answer, ')')
    return

if __name__ == '__main__':
    prompt = "\nEnter 'y' to play again.\n(All other characters will quit.)\n>>> "
    while True:
        print("-"*79)
        guessSubnet()
        choice = input(prompt)
        if not choice.lower() == 'y':
            break
