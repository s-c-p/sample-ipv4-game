takes github.com/agraymd/pythonSubnetGuessingGame  
and makes it webapp

## How to run?
1. `pip install bottle`
2. download this repo as zip, unzip it and open shell in the folder where you just unzipped it
3. `python server.py`
4. open web browser at the indicated address
5. play :)